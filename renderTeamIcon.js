const _ = require('lodash')
const htmlToElement = require('./htmlToElement.js')
const teamIcon = require('./templates/teamIcon.js')
const addTeamIconEvents = require('./addTeamIconEvents.js')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance)) 
        throw new Error('Invalid instance')

    if(!(args.target instanceof Element))
        throw new Error('Invalid target')

    const icon = teamIcon()
    const element = htmlToElement(icon)
    
    if(_.isArray(args.classList)){
        _.forEach(args.classList, function(v){
            element.classList.add(v)
        })
    }

    args.target.appendChild(element)
    args.instance.props.elements.icons.push(element)

    addTeamIconEvents({
        element: element,
        instance: args.instance
    })
    
    return element
}